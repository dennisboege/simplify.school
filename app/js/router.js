
app.config(function($routeProvider, $locationProvider){
    $locationProvider.hashPrefix('!');
    $routeProvider.
    when('/home',{templateUrl : 'pages/home.html'}).
    when('/contact',{templateUrl : 'pages/contact.html'}).
    when('/imprint',{templateUrl : 'pages/imprint.html'}).
    otherwise({redirect : '/home',templateUrl : 'pages/home.html'});
});